# [saintwladimir2013.cae.li](https://saintwladimir2013.cae.li)

Site du camp national SUF de la Saint Wladimir 2013, à Pré en Paille en Mayenne.
Un grand moment de scoutisme !

Le site héberge notamment les archives PDF du Pélican Noir, glorieux journal de
résistance, référence incontournable du royaume.
