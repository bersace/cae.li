FQDN?=$(notdir $(CURDIR))
SSH=ssh doncoeur.cae.li
VOLUMES_ROOT?=/var/externe

export DOMAIN=$(FQDN:.cae.li=)
export HOSTNAME=$(DOMAIN)
export COMPOSE_PROJECT_NAME=$(DOMAIN)
export VOLUMES=$(VOLUMES_ROOT)/$(FQDN)
