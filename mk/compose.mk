#
# Règles de bases pour gérer un site avec compose
#

COMPOSE_FILE?=docker-compose.yml
aide:
	@echo
	@cat README.md
	@echo
	@gawk 'match($$0, /([^:]*):.+#'': (.*)/, m) { printf "    %-16s%s\n", m[1], m[2]}' $(MAKEFILE_LIST) | sort
	@echo

env:
	@echo export COMPOSE_FILE=$(COMPOSE_FILE)
	@echo export COMPOSE_PROJECT_NAME=$(COMPOSE_PROJECT_NAME)
	@echo export DOMAIN=$(DOMAIN)
	@echo export FQDN=$(FQDN)
	@echo export HOSTNAME=$(HOSTNAME)
	@echo export VOLUMES=$(VOLUMES)

purge:
	@echo Êtes vous sûr ?
	@read _
	docker-compose down -v
	$(SSH) sudo rm -rvf $(VOLUMES)

RSYNC=rsync -av --rsync-path='/usr/bin/sudo /usr/bin/rsync'
push:  #: Pousse les données en ligne.
	$(RSYNC) volumes/ $(FQDN):$(VOLUMES)/

sync:  #: Pousse et purge les données en ligne.
	$(RSYNC) --delete volumes/ $(FQDN):$(VOLUMES)/

backup:  #: Récupère les données en lignes
	$(RSYNC) --delete $(FQDN):$(VOLUMES)/ volumes/

stop:
	docker-compose $@

up:
	docker-compose $@ -d

# Tester la configuration nginx. Recharger avec docker-compose restart nginx
test: push
	docker-compose run --rm nginx nginx -t

# Recharger nginx après avoir testé la configuration
reload: test
	docker-compose exec nginx nginx -s reload
