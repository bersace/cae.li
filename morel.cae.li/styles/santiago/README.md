# Style Santiago

Ce style strass a été conçu et réalisé en 2008 avec Arnaud de RUFFRAY. Tout
comme les styles de Strass, ce style est écrit en SCSS et généré avec `libsass`
et `webassets`. Un `Pipfile` contient tout ce qu'il faut à `pipenv` pour
installer ce qu'il faut.

Pour mettre en ligne le style: `make push`.
