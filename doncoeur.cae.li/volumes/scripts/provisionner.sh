#!/bin/bash -eux

cat > /etc/systemd/system/docker-prune.service <<EOF
[Unit]
Description=Purge des données docker

[Service]
Type=oneshot
ExecStart=/bin/bash -o pipefail -c 'docker images -f dangling=true -q | xargs -rt docker rmi'
ExecStart=/usr/bin/docker volume prune --force
ExecStart=/usr/bin/docker network prune --force
EOF

cat > /etc/systemd/system/docker-prune.timer <<EOF
[Unit]
Description=Purger les données docker toute les semaines

[Timer]
OnCalendar=Sun *-*-* 00:00:00

[Install]
WantedBy=timers.target
EOF

systemctl daemon-reload
systemctl enable --now docker-prune.timer
