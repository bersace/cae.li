# cae.li

Ce projet contient le code et la configuration pour déployer le serveur
`doncoeur.cae.li` hébergeant les services de https://cae.li/.


## Administrer les services

J'utilise `direnv` pour définer les variables d'env pour `docker` et `ovh`.

``` bash
$ cat .envrc
eval $(docker-machine env mantienne)
export OVH_APPLICATION_KEY=...
export OVH_APPLICATION_SECRET=...
export OVH_CONSUMER_KEY=...
export MAILGUN_SMTP_PASSWORD=...
export MAILJET_SMTP_USERNAME=...
export MAILJET_SMTP_PASSWORD=...

$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                      NAMES
4892555c403c        nginx:alpine        "nginx -g 'daemon ..."   16 hours ago        Up 2 hours          0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   mantienne_nginx_1
47aa12e0d564        alterrebe/postfix-relay   "/root/run.sh"           About an hour ago   Up About an hour    25/tcp                                     mantienne_smtp_1
```

Chaque domaine a son propre dossier avec:

- un `README`.
- un `docker-compose.yml` définissant les services.
- un dossier `volumes/` poussé dans `/var/volumes_b/<service>.cae.li/`.
- un `Makefile` stockant les commandes usuelles.

Dans chaque dossier, j'ajoute un `.envrc` avec `eval $(make env)`. L'idée
est que le nom du projet Compose soit le sous-domaine du service.


# Nomenclature

Puisqu'on nomme les rues et les places d'après des grands personnages, j'ai
choisi de nommer les serveurs de `cae.li` d'après des grands scouts.

[**R.P. Paul DONCŒUR**](https://fr.scoutwiki.org/Paul_Donc%C5%93ur). Jésuite, il
œuvre particulièrement à la naissance de la Route Scout de France dès 1924 et
jusqu'au milieu de la seconde guerre mondiale. Il initia également une pastorale
nouvelle pour les foyers, qui inspirera le Père CAFFAREL pour fonder les Équipes
Notre-Dame. `doncoeur.cae.li` est un VM Debian sur l'OpenStack d'OVH.
`doncoeur.cae.li` a remplacé `mantienne.cae.li` en mai 2021 après l'incendie de
SBG.

[**Bernard MANTIENNE**](https://fr.scoutwiki.org/Bernard_Mantienne). Directeur
de la revue unitaire Raid. Cofondateur des SUF. `mantienne.cae.li` est une VM
CoreOS dans l'OpenStack d'OVH, initialisé avec `docker-machine`.
`mantienne.cae.li` a remplacé `sevin.cae.li` en octobre 2017.

[**Vénérable Jacques SEVIN**](https://fr.scoutwiki.org/Jacques_Sevin). Fondateur
du scoutisme catholique, poète. `sevin.cae.li` était une VM Jessie chez OVH à la
suite de `delsuc.cae.li`. Le premier serveur de `cae.li` se nommait également
`sevin.cae.li`, c'était une VM chez Gandi.

[**Pierre DELSUC**](https://fr.scoutwiki.org/Pierre_Delsuc). Un des premiers
scouts de France. Il rédigea notamment Étapes après l'abandon de la pédagogie
unitaire par les Scouts de France. `delsuc.cae.li` était un VPS chez Gandi à la
suite de `menu.cae.li`.

[**Paul COZE**](https://fr.scoutwiki.org/Paul_Coze). Un des premiers Scouts de
France. Il introduisit la totémisation et beaucoup de techniques indiennes.
`coze.cae.li` était une VM Xen chez Server Axis, en Amérique, à la même époque
que `menu.cae.li`.

[**Michel MENU**](https://fr.scoutwiki.org/Michel_Menu). Résistant, créateur des
foulards noirs et des raiders. Rédacteur des *Bases fondamentales du scoutisme*.
`menu.cae.li` était un VPS chez Gandi.


*Lux aeterna luceat eis, Domine, cum sanctis tuis in æternum, quia pius es.*


## Références

- https://www.ovh.com/fr/g934.premiers_pas_avec_lapi
- Explorateur de l'API OVH: https://api.ovh.com/console/
- https://github.com/ovh/python-ovh
- [Documentation de Docker
  Compose](https://docs.docker.com/compose/compose-file/)
