# [lounge.cae.li](https://lounge.cae.li)

Client IRC web persistent. Remplace irssi, car docker se prête mal aux
interfaces texte.

Il faut créer manuellement les utilisateurs avec `docker-compose run --rm lounge
lounge add <nom>`, etc. Cf. la [documentation de
lounge](https://thelounge.github.io/docs/getting_started/usage.html).
