default:

prune-images:
	docker images -f dangling=true -q | xargs -rt docker rmi

EACH_MAKE=xargs -rtl -I% -P8 make -C %
EACH_STRASS_MAKE=ls *.cae.li/Makefile | xargs grep -l 'include ../strass' | xargs dirname | $(EACH_MAKE)
strass-%:
	docker pull bersace/strass:latest
	$(EACH_STRASS_MAKE) $*
	$(MAKE) prune-images

EACH_NGINX_MAKE=ls *.cae.li/docker-compose.yml | xargs grep -l 'nginx:alpine' | xargs dirname | $(EACH_MAKE)
nginx-upgrade:
	docker pull nginx:alpine
	$(EACH_NGINX_MAKE) up
	$(MAKE) prune-images

up stop:
	$(MAKE) -C mantienne.cae.li $@
	$(MAKE) -C bersace.cae.li $@
	$(MAKE) -C etiennethais.cae.li $@
	$(MAKE) -C saintwladimir2013.cae.li $@
	$(MAKE) -C suf1520.cae.li $@
	$(MAKE) -C straphael.cae.li $@
	$(MAKE) -C morel.cae.li $@
	$(MAKE) -C sufstlouis.cae.li $@
	$(MAKE) -C supervision.cae.li $@
	$(MAKE) -C lounge.cae.li $@
	$(MAKE) -C bacasable.cae.li $@
