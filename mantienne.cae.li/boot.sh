#!/bin/bash -eux

start() {
	local containers

	readarray -t containers <<<"$(docker ps --quiet --all --filter status=exited "$@")"
	[ -z "${containers[*]}" ] && return
	docker start "${containers[@]}"
}

start --filter label=com.docker.compose.project=mantienne
start
