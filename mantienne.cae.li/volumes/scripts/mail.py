#!/usr/bin/env python

import logging
import smtplib


logging.basicConfig(level=logging.DEBUG)


msg = """\
From: mailjet@cae.li
To: bersace+to@gmail.com

Hello!"""

with smtplib.SMTP('smtp', 25) as server:
    server.sendmail("mailjet@cae.li", "bersace+to@gmail.com", msg)
