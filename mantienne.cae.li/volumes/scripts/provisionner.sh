#!/bin/bash -eux

VOLUMES=/var/volumes_b/mantienne.cae.li

cat > /etc/systemd/system/docker-prune.service <<EOF
[Unit]
Description=Purge des données docker

[Service]
Type=oneshot
ExecStart=/bin/bash -o pipefail -c 'docker images -f dangling=true -q | xargs -rt docker rmi'
ExecStart=/usr/bin/docker volume prune --force
ExecStart=/usr/bin/docker network prune --force
EOF

cat > /etc/systemd/system/docker-prune.timer <<EOF
[Unit]
Description=Purger les données docker toute les semaines

[Timer]
OnCalendar=Sun *-*-* 00:00:00
EOF


cat > /etc/systemd/system/certbot-renew.service <<EOF
[Unit]
Description=Renouveler le certificat Let's Encrpyt

[Service]
Type=oneshot
ExecStart=/usr/bin/docker run --rm -v ${VOLUMES}/certbot/etc/:/etc/letsencrypt -v ${VOLUMES}/certbot/webroot:/var/webroot certbot/certbot:latest renew -n
ExecStart=/usr/bin/docker exec mantienne_nginx_1 nginx -s reload
EOF

cat > /etc/systemd/system/certbot-renew.timer <<EOF
[Unit]
Description=Renouveller le certificat Let's Encrypt tout les trois mois

[Timer]
OnCalendar=*-01,04,07,10-* 00:00:00
EOF

systemctl daemon-reload
systemctl start docker-prune.timer
systemctl start certbot-renew.timer
