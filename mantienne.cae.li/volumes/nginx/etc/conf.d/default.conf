ssl_certificate /etc/letsencrypt/live/cae.li/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/cae.li/privkey.pem;
ssl_trusted_certificate /etc/letsencrypt/live/cae.li/chain.pem;

ssl_session_cache shared:SSL:20m;
ssl_session_timeout 180m;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;
ssl_dhparam /etc/nginx/dhparam.pem;
ssl_stapling on;
ssl_stapling_verify on;

add_header Strict-Transport-Security "max-age=31536000";

server {
  listen 80;
  server_name *.cae.li cae.li;

  location /.well-known/acme-challenge/ {
    root /usr/share/nginx/certbot;
  }

  location / {
    return 301 https://$host$request_uri;
  }
}

server {
  listen 443 ssl http2;
  server_name cae.li;

  location / {
    root   /usr/share/nginx/html;
    try_files /index.html /index.html;
  }
}

server {
  listen 443 ssl http2;
  server_name ~(?P<service>.+).cae.li;

  error_page 502 503 504 @fallback;

  location @fallback {
    root   /usr/share/nginx/html;
    try_files /index.html /index.html;
  }

  location / {
    client_max_body_size 16M;

    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    proxy_send_timeout 2s;
    proxy_pass http://$service.backend;
  }
}
