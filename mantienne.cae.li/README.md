# mantienne.cae.li

Les services de base du serveur: nginx et certbot.

Pour *initialiser le service* : `make volumes dhparam up`

Pour *tester* la conf nginx: `make test`.

Pour *redémarrer* nginx: `docker-compose restart nginx`.


## Ajouter un site web

- Ajouter le nom du site dans `volumes/certbot/etc/cli.ini`.
- Regénérer le certificat avec `make cert`.
- Lancer un conteneur dans le réseau docker `mantienne_default` avec le nom
  `acme.backend`.
- Le site est accessible à `https://acme.cae.li`.
- L'app peut envoyer des mails `@cae.li` à l'adresse `smtp` sur le port 25.


## Certificat autosigné

Pour générer le certificat autosigné utilisé par le serveur SMTP:

``` console
# openssl req  -new -x509 -days 3650 -nodes -sha256 -out /etc/ssl/certs/ssl-cert-snakeoil.pem -keyout /etc/ssl/private/ssl-cert-snakeoil.key
```
