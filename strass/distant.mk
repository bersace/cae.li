#
# Règles de bases pour gérer une instance strass
#

strass_default: default
	@echo
	@echo
	@cat ../strass/README.md

include ../mk/vars.mk
include ../mk/compose.mk

export COMPOSE_FILE=$(shell readlink -e ../strass/docker-compose.yml)

fixperms:
	ssh mantienne.cae.li sudo chgrp -R core $(VOLUMES)
	ssh mantienne.cae.li sudo find $(VOLUMES) -type d -exec 'chmod g+rwx {} \;'
	ssh mantienne.cae.li sudo find $(VOLUMES) -type f -exec 'chmod g+rw {} \;'
	ssh mantienne.cae.li sudo find $(VOLUMES) -type d '!' -path '*/private/*'  -exec 'chmod o+rx {} \;'
	ssh mantienne.cae.li sudo find $(VOLUMES) -type f '!' -path '*/private/*' -exec 'chmod o+r {} \;'
	docker-compose run --rm strass $@

local: styles
	mkdir -p $@
	ln -fs ../../strass/local.mk $@/Makefile
	echo 'eval $$(make env)' > $@/.envrc

migrate:
	docker-compose run --rm strass setmaint snapshot migrate unsetmaint

push: volumes/nginx/default.conf

recreate:
	docker-compose rm --stop --force -v strass
	docker-compose up -d strass

restore:
	docker-compose run --rm strass setmaint restore unsetmaint

update:
	docker-compose up -d strass

upgrade:
	$(MAKE) update
	$(MAKE) migrate
	$(MAKE) statics
	$(MAKE) reload

setmaint statics unsetmaint purgesession sessionclean:
	docker-compose run --rm strass $@

styles:
	mkdir -p $@
	ln -fs ../../strass/Makefile.styles $@/Makefile
	@echo source_env .. > $@/.envrc

volumes/nginx/default.conf: ../strass/nginx.conf
	mkdir -p $(shell dirname $@)
	sed 's/$${COMPOSE_PROJECT_NAME}/$(COMPOSE_PROJECT_NAME)/' < $^ > $@
