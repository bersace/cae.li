# Administration de strass

Déployer strass en prod est fait assez simplement avec *Docker Compose*. Pour
faciliter le développement de style personnalisé, on a deux sous-projets
associés : un dossier `local/` avec les règles `local.mk`, un dossier `styles/`
avec les règles de `styles.mk`. Les styles sont déployés indépendament du site.

Sur le serveur, les fichiers appartiennent à l'utilisateur `strass` dans le
conteneur et au groupe `core` sur l'hôte. Les fichiers sont accessible depuis
`other` pour êtres servis par nginx, sauf `private/`. Des règles `fixperms`
permettent à chaque partie de définir les droits et attributions pour leurs
fonctionnement.


## Rappatrier et lancer le site en local

- D'abord, dans le projet `.cae.li/`, corriger les permissions avec `make fixperms`.
- Créer le projet local avec `make local` et `cd local/`.
- Rappatrier les données avec `make pull`.
- Lancer le projet avec `make up`.
- La page s'ouvre dans votre navigateur.

Un bandeau `TEST` distingue la version locale de la production. Les mails sont
détournés dans `volumes/strass/private/mails/`. `thunderbird -file .../*.eml`
permet de lir un fichier email.


## Créer un style

- Requiert un projet local pour tester le style. Le dossier `styles/` est créé à
  côté de `local/`.
- Créer un dossier avec votre style dedans.
- Mettre en ligne le style avec `make push`.


## Opérations usuelles

`make env` code d'export de variables à injecter dans `.envrc`.

`make setmaint`/`make unsetmaint` mettre en maintenance.

`make up` pousser la configuration nginx et (re)lancer les conteneurs.

`make update` : mettre à jour strass, sans migration.

`make upgrade` : mettre à jour l'image, le conteneur et migrer les données.
