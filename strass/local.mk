#
# Makefile à utiliser dans pouet.cae.li/local/
#

FQDN=$(shell basename $$(readlink -e ..))
include ../../mk/vars.mk

export COMPOSE_FILE=../../strass/docker-compose.local.yml

# Forcer l'utilisation du moteur docker local
undefine DOCKER_TLS_VERIFY
undefine DOCKER_HOST
undefine DOCKER_CERT_PATH
undefine DOCKER_MACHINE_NAME

default:

# Pour .envrc
env:
	@echo export COMPOSE_FILE=$(COMPOSE_FILE)
	@echo export COMPOSE_PROJECT_NAME=$(HOSTNAME)
	@docker-machine env --unset | grep -v '^#'

fixperms:
	find . -type d | xargs chmod g+rwx
	find . -type f | xargs chmod g+rw

statics:
	docker-compose run --rm strass $@

pull:
	mkdir -p volumes/
	rsync --verbose --recursive --delete --exclude cache/ --exclude snapshot/ mantienne.cae.li:$(VOLUMES)/ volumes/

purge:
	docker-compose down -v

up:
	docker-compose up -d
	@echo Ouverture de http://$(HOSTNAME).strass.docker:8000/
	@echo sleep 3
	@xdg-open http://$(HOSTNAME).strass.docker:8000/
