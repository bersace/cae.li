default:

fixperms:
	find . -type d | xargs chmod g+rwx
	find . -type f | xargs chmod g+rw

push:
	rsync --recursive --no-times --delete --exclude src/ ./ mantienne.cae.li:$(VOLUMES)/strass/data/styles/
	docker-compose run --rm strass fixperms
